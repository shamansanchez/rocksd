package main

import (
	"gitlab.com/shamansanchez/rocksd/cmd"
)

func main() {
	cmd.Execute()
}
