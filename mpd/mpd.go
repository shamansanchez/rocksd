package mpd

import (
	"bytes"
	_ "embed"
	"text/template"
)

type Config struct {
	Name            string
	Port            int
	Description     string
	Password        string
	WindowSize      int
	DisableAshuffle bool
	Outputs         []struct {
		Host      string
		Port      int
		Format    string
		MountName string
		Quality   int
	}
}

//go:embed mpd.conf.tmpl
var templateString string

func GetMPDConfig(conf *Config) (string, error) {
	tmpl, err := template.New("mpd.conf.tmpl").Parse(templateString)
	if err != nil {
		return "", err
	}

	b := []byte{}
	output := bytes.NewBuffer(b)

	err = tmpl.Execute(output, conf)
	if err != nil {
		return "", err
	}

	return output.String(), nil
}
