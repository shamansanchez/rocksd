package util

import (
	"fmt"
	"os"
)

func OrDie(err error, fmtString string, args ...interface{}) {
	if err != nil {
		message := fmt.Sprintf(fmtString, args...)
		fmt.Printf("ERROR: %s: %v\n", message, err.Error())
		os.Exit(1)
	}
}
