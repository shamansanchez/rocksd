package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/shamansanchez/rocksd/mpd"
	"gitlab.com/shamansanchez/rocksd/util"
)

var (
	cfgFile string
	conf    mpd.Config

	rootCmd = &cobra.Command{
		Use: "rocksd",
		Run: func(cmd *cobra.Command, args []string) {
			mpdConf, err := mpd.GetMPDConfig(&conf)
			util.OrDie(err, "couldn't get MPD config")

			runtimeDir := os.Getenv("XDG_RUNTIME_DIR")

			if runtimeDir == "" {
				runtimeDir = "/tmp"
			}

			confPath := fmt.Sprintf("%s/rocksd.%s.mpd.conf", runtimeDir, conf.Name)

			err = os.WriteFile(confPath, []byte(mpdConf), 0644)
			util.OrDie(err, "couldn't create MPD config file")

			defer os.Remove(confPath)

			go StartMPD(confPath)

			if conf.WindowSize == 0 {
				conf.WindowSize = 7
			}

			if !conf.DisableAshuffle {
				go StartAshuffle(fmt.Sprint(conf.Port), fmt.Sprint(conf.WindowSize))
			}

			for {
				time.Sleep(time.Second)
			}
		},
	}
)

func Execute() {
	rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file")
}

func initConfig() {
	if cfgFile == "" {
		fmt.Println("No config file specified")
		rootCmd.Usage()
		os.Exit(1)
	}
	viper.SetConfigFile(cfgFile)
	viper.SetConfigType("yaml")

	err := viper.ReadInConfig()
	util.OrDie(err, "failed to load config")

	err = viper.Unmarshal(&conf)
	util.OrDie(err, "failed to load config file %s", cfgFile)

}
