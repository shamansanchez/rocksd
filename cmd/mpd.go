package cmd

import (
	"bufio"
	"fmt"
	"os/exec"
	"time"

	"gitlab.com/shamansanchez/rocksd/util"
)

func StartMPD(confFile string) error {
	mpdPath, err := exec.LookPath("mpd")
	util.OrDie(err, "couldn't find mpd")

	for {
		cmd := exec.Command(mpdPath, "--stdout", "--no-daemon", confFile)
		BackgroundCommand(cmd, "MPD")

		time.Sleep(time.Second)
	}
}

func StartAshuffle(port string, windowSize string) error {
	ashufflePath, err := exec.LookPath("ashuffle")
	util.OrDie(err, "couldn't find ashuffle")

	for {
		cmd := exec.Command(ashufflePath, "-p", port, "-q", "1", "-t", fmt.Sprintf("window-size=%s", windowSize))
		BackgroundCommand(cmd, "ashuffle")

		time.Sleep(time.Second)
	}
}

func BackgroundCommand(cmd *exec.Cmd, cmdName string) {
	fmt.Printf("Starting %s...\n", cmdName)
	out, err := cmd.StdoutPipe()
	util.OrDie(err, "couldn't open stdoutpipe for %s", cmdName)

	outErr, err := cmd.StderrPipe()
	util.OrDie(err, "couldn't open stderrpipe for %s", cmdName)

	scanner := bufio.NewScanner(out)
	errScanner := bufio.NewScanner(outErr)

	err = cmd.Start()
	util.OrDie(err, "couldn't start %s", cmdName)

	go func() {
		for scanner.Scan() {
			fmt.Printf("[%s] %s\n", cmdName, scanner.Text())
		}
		fmt.Printf("scanner died for %s...\n", cmdName)
	}()

	go func() {
		for errScanner.Scan() {
			fmt.Printf("[%s err] %s\n", cmdName, errScanner.Text())
		}
		fmt.Printf("errScanner died for %s...\n", cmdName)
	}()

	cmd.Wait()
}
